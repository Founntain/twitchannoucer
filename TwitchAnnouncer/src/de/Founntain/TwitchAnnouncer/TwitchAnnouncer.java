package de.Founntain.TwitchAnnouncer;

import java.io.File;
import java.util.ArrayList;

import org.bukkit.plugin.java.JavaPlugin;


public class TwitchAnnouncer extends JavaPlugin{
	
	public HelperClass Helper = new HelperClass(this);
	public ArrayList<Stream> Streams = new ArrayList<>();
	public String Token;
	
	@Override
	public void onEnable() {
		Helper.sendMessage("Initializing TwitchAnnouncer");
		
		loadConfig();
		
		this.getCommand("ta").setExecutor(new CommandTwitchAnnouncer(Helper));
	}
	
	@Override
	public void onDisable() {
		
	}
	
	private void loadConfig() {
		this.getConfig();
		if(!new File("plugins/TwitchAnnouncer/config.yml").exists()) {
			Helper.sendErrorToConsole("config.yml doesn't exist, creating new one");
			this.saveDefaultConfig();
		}else
			Helper.sendMessage("§aConfigfile already existing, moving on");
	}
}
