package de.Founntain.TwitchAnnouncer.CommandParameters;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import de.Founntain.TwitchAnnouncer.HelperClass;

public class Parameter {
	public CommandSender Sender;
	public Command Command;
	public String Label;
	public String[] Args;
	public HelperClass Helper;
	
	
	public Parameter(CommandSender sender, Command command, String label, String[] args, HelperClass helper) {
		Sender = sender;
		Command = command;
		Label = label;
		Args = args;
		Helper = helper;
	}
}
