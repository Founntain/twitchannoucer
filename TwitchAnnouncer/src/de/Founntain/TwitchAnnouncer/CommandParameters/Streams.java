package de.Founntain.TwitchAnnouncer.CommandParameters;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import de.Founntain.TwitchAnnouncer.HelperClass;
import de.Founntain.TwitchAnnouncer.Stream;

public class Streams extends Parameter implements ICommand{

	private String Token;
	
	public Streams(CommandSender sender, Command command, String label, String[] args, HelperClass helper) {
		super(sender, command, label, args, helper);
	}
	
	public boolean doCommand() {
		
		Token = Helper.getConfig().getString("token");
		
		if(Token.equals("null") || Token.length() <= 0) {
			Helper.sendErrorToConsole("Kein Token angebeben in der config.yml");
			return false;
		}
		
		Helper.TwitchAnnouncer.Streams.clear();
		
		for(String s : Helper.getConfig().getStringList("channels")) {		
			URL url;
		
			try {
				Helper.sendMessage("Trying to get stream data from "+ s);
				
				url = new URL("https://api.twitch.tv/helix/streams?user_login=" + s);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestProperty("Client-ID", Token);
				
				if(con.getResponseCode() != HttpsURLConnection.HTTP_OK) {
					Helper.sendErrorToConsole("Request Failed: HTTP Error Code: "+ con.getResponseCode());
					return false;
				}
			
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				StringBuffer json = new StringBuffer();
			
				String line;
				while((line = br.readLine()) != null) {
					json.append(line);
				}
			
				JSONParser parser = new JSONParser();
				
				try {
					
					Object obj = parser.parse(json.toString());
					
					JSONObject jsonObject = (JSONObject) obj;
					
		            // loop array
		            JSONArray msg = (JSONArray) jsonObject.get("data");
		            
		            @SuppressWarnings("unchecked")
		            Iterator<JSONObject> iterator = msg.iterator();
		            JSONObject result = iterator.next();
		            
		            Helper.TwitchAnnouncer.Streams.add(new Stream(s, result.get("id").toString(), result.get("user_id").toString(), result.get("game_id").toString(), result.get("type").toString(), result.get("title").toString(), Integer.parseInt(result.get("viewer_count").toString())));
		            
				}catch(Exception ex) {
					Helper.TwitchAnnouncer.Streams.add(new Stream(s));
					continue;
				}
				
				con.disconnect();
			}catch(Exception ex) {
				Helper.sendErrorToConsole(ex.toString());
				return false;
			}
		}
		
		for(Stream s : Helper.TwitchAnnouncer.Streams) {
			for(Player p : Bukkit.getOnlinePlayers()) {
				p.sendMessage(s.toString());
			}
		}
		
		return true;
	}
	
}
