package de.Founntain.TwitchAnnouncer.CommandParameters;

import org.bukkit.command.*;
import de.Founntain.TwitchAnnouncer.*;

public class Help extends Parameter implements ICommand{
	
	public Help(CommandSender sender, Command command, String label, String[] args, HelperClass helper) {
		super(sender, command, label, args, helper);
	}
	
	public boolean doCommand() {
		Helper.sendMessageToClient("/ta reload => Reloads the config for TwitchAnnouncer", Sender);
		return true;
	}
	
}
