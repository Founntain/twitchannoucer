package de.Founntain.TwitchAnnouncer.CommandParameters;

import org.bukkit.command.*;

import de.Founntain.TwitchAnnouncer.HelperClass;

public class Reload extends Parameter implements ICommand{
	
	public Reload(CommandSender sender, Command command, String label, String[] args, HelperClass helper) {
		super(sender, command, label, args, helper);
	}
	
	public boolean doCommand() {
		
		if(Sender.isOp())
			Helper.reloadConfig();
		else {
			Helper.sendErrorToClient("You don't have enough permissions to do that", Sender);
			return false;
		}
		
		Sender.sendMessage("�aConfig reloaded");
		
		return true;
	}
}
