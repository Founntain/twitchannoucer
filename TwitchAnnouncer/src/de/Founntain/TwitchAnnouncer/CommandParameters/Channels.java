package de.Founntain.TwitchAnnouncer.CommandParameters;

import org.bukkit.command.*;

import de.Founntain.TwitchAnnouncer.*;

public class Channels extends Parameter implements ICommand{
	
	public Channels(CommandSender sender, Command command, String label, String[] args, HelperClass helper) {
		super(sender, command, label, args, helper);
	}
	
	public boolean doCommand() {
		for(String s : Helper.getConfig().getStringList("channels")) {
			Helper.sendMessage("Found channel: " + s);
		}
		return true;
	}
	
}
