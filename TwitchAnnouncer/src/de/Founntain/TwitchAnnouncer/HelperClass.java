package de.Founntain.TwitchAnnouncer;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.configuration.file.FileConfiguration;

public class HelperClass {
	
	//Attributes
	public ConsoleCommandSender CommandSender;
	public TwitchAnnouncer TwitchAnnouncer;
	private String Header = "�e[TwitchAnnouncer] �f";
	
	
	//Constructor
	public HelperClass() {
		CommandSender = Bukkit.getConsoleSender();
	}
	
	public HelperClass(TwitchAnnouncer t) {
		TwitchAnnouncer = t;
		
		CommandSender = Bukkit.getConsoleSender();
	}
	
	//Methods	
	public void sendMessage(String message) {
		CommandSender.sendMessage(Header + message);
	}
	
	public void sendMessageToClient(String message, CommandSender sender) {
		sender.sendMessage(message);
	}
	
	public void sendErrorToConsole(String message) {
		CommandSender.sendMessage(Header + "�c" + message);
	}
	
	public void sendErrorToClient(String message, CommandSender sender) {
		sender.sendMessage("�c[Error] "+ message);
	}
	
	public void sendErrorToClientAndConsole(String message, CommandSender sender) {
		sender.sendMessage("�c[Error] "+ message);
	}
	
	public FileConfiguration getConfig() {
		return TwitchAnnouncer.getConfig();
	}
	
	public void reloadConfig() {
		TwitchAnnouncer.reloadConfig();
	}
}
