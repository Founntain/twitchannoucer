package de.Founntain.TwitchAnnouncer;

public class Stream {
	HelperClass Helper;
	
	public String name;
	public String id;
	public String user_id;
	public String game_id;
	public String type;
	public String title;
	public int viewer_count;
	
	public Stream(String name) {
		this.name = name;
		this.type = null;
	}
	
	public Stream(String name, String id, String userId, String gameId, String type, String title, int viewer_count) {
		this.name = name;
		this.id = id;
		user_id = userId;
		game_id = gameId;
		this.type = type;
		this.title = title;
		this.viewer_count = viewer_count;
	}
	
	@Override
	public String toString() {
		if(this.type != null)
			return name + " is "+ type + " with �5" + viewer_count + "�f Viewer!";
		else
			return name + " is not live!";
	}
}
